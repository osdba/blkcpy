#############make file for tangtest###################

PLATFORM=$(shell uname)

ifeq ($(PLATFORM),AIX)
    CC=gcc
	MAKE=make
	CFLAGS=$(DEBUG) -static -maix64 -D_LARGE_FILES -Dthread_creds_t=''
endif

ifeq ($(PLATFORM),Linux)
	CC=gcc
	MAKE=make
	CFLAGS=$(DEBUG) -static
endif

ifeq ($(PLATFORM),SunOS)
	CC=gcc
	MAKE=make
	CFLAGS=$(DEBUG) -static
endif

ifeq ($(PLATFORM),HP)
	CC=aCC
	CFLAGS=-D_REENTRANT -w -g +p
	MAKE=make
endif

#����Ӧ��#
blockcopy:
	$(CC) $(DEBUG) $(CFLAGS) blkcpy.c -o blkcpy

debug:
	$(MAKE) $(MAKEFILE) DEBUG="-g -DDEBUG"
clean: 
	rm  blkcpy
