## 介绍

有人问有dd为什么还需要这个工具。这个工具拷贝时不会象dd，会截断文件，具体说明见后面的说明。

## 使用

使用此工具，可以把块设备其中的一部分内容拷贝成一个文件，也可以拷贝到另一个块设备中。 

源码目录直接运行make就可以完成编译，生成blkcpy可执行文件，不带任何参数运行这个文件会出现一个帮助，

```
Syntax:  
         blkcpy <sourcefile> <startpos> <readbytes> <destfile> <startpos>
example:
    blkcpy /dev/rora_temp 4k 50M /tmp/ora_temp.dat 0
    blkcpy /dev/rora_temp 4k 2G  /dev/rora_temp2 4k      
    blkcpy /dev/rora_temp 4k end /tmp/ora_temp
```

dd工具会截断文件的说明： 如我们要把一个50M的源文件t1.dat第23Mbytes开始的1M数据拷贝到另一个文件t2.dat的第24M开始的位置，其它数据不改动。如果执行dd命令：

```
osdba@osdba-laptop:~/tmp$ ls -l
total 102400
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:16 t1.dat
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:16 t2.dat
osdba@osdba-laptop:~/tmp$ dd if=t1.dat bs=1M skip=23 of=t2.dat seek=24 count=1
1+0 records in
1+0 records out
1048576 bytes (1.0 MB) copied, 0.00137634 s, 762 MB/s
osdba@osdba-laptop:~/tmp$ ls -l 
total 76800
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:16 t1.dat
-rw-rw-r-- 1 osdba osdba 26214400 Jun 29 00:19 t2.dat
osdba@osdba-laptop:~/tmp$ 
```

最后会发现t2.dat文件从50M被截取成了24M，也就是说最后25M数据被dd给截断了。而使用我写的这个blkcpy工具则不会发生这种情况:

```
osdba@osdba-laptop:~/tmp$ ls -l
total 102400
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:16 t1.dat
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:22 t2.dat
osdba@osdba-laptop:~/tmp$ blockcopy t1.dat 23M 1M t2.dat 24M
blockcopy v0.1 - tangcheng 2009.03.12
Finished:00000001M
osdba@osdba-laptop:~/tmp$ ls -l 
total 102400
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:16 t1.dat
-rw-rw-r-- 1 osdba osdba 52428800 Jun 29 00:23 t2.dat
```

同时使用我这个工具，可以方便指定起始位置，拷贝的长度，写入目标文件的位置，比dd更容易操作。

