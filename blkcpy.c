//linux:gcc -p -g  blkcpy.c -o blkcpy
//aix:gcc -p -g -pthread -static -maix64 -D_REENTRANT -Dthread_creds_t='' blkcpy.c -o blkcpy
//aix:gcc -p -g -pthread -static -D_LARGE_FILES  -D_REENTRANT -Dthread_creds_t='' blkcpy.c -o blkcpy

/** ============================================================================
** Author:             TangCheng                (Feb 2009)
** Email:              chengdata@gmail.com
** Current maintainer: TangCheng                (Feb 2023)
** Email:              chengdata@gmail.com
** ----------------------------------------------------------------------------
** Copyright (C) 2008-2022 TangCheng
**
** This program is free software; you can redistribute it and/or modify it
** under the terms of the GNU General Public License as published by the
** Free Software Foundation; either version 2, or (at your option) any
** later version.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
** See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
** ----------------------------------------------------------------------------*/

#undef   _FILE_OFFSET_BITS   
#define  _FILE_OFFSET_BITS   64   

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
  
#define BLOCKSIZE (1024*1024)

off_t GetArgValue(char * szValue);
int main(int argc,char * argv[])
{
    off_t lSourceStartPos,lTotalReadBytes,lDestStartPos;
	long lUnit;
	long lBlockSize;
	off_t lReadSize;
	int iLen;
	int rfd,wfd;
	off_t pos;
	int iRet;
	char * buf;
	off_t i;
	
	printf("blockcopy v0.1 - tangcheng 2009.03.12\n");
	if(argc!=6)
	{
	    goto cmdparaerror;
	}
	
    lSourceStartPos=GetArgValue(argv[2]);
	if(lSourceStartPos==-1)
	{
	    goto cmdparaerror;
	}
	
	if(!strcmp(argv[3],"end"))
	{
	    lTotalReadBytes=0x7FFFFFFFFFFFFFFEll;
	}
	else
	{
        lTotalReadBytes=GetArgValue(argv[3]);
	    if(lTotalReadBytes==-1)
	    {
	        goto cmdparaerror;
	    }
    }
	
	lDestStartPos=GetArgValue(argv[5]);
	if(lDestStartPos==-1)
	{
	    goto cmdparaerror;
	}
	
#ifdef _AIX
    rfd=open(argv[1],O_RDONLY);
#else
	rfd=open(argv[1],O_RDONLY);
#endif

	if(rfd==-1)
	{
	     printf("Can not open file %s\n",argv[1]);
		 return -1;
	}

#ifdef _AIX
    wfd=open(argv[4],O_CREAT|O_WRONLY,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
#else	
	wfd=open(argv[4],O_CREAT|O_WRONLY);
#endif
	
	if(wfd==-1)
	{
	    printf("Can not create or open file %s\n",argv[4]);
		return -1;
	}
	
	pos=lseek(rfd,lSourceStartPos,SEEK_SET);
	if(pos==-1)
	{
	    printf("File %s can not move pointer to %ld\n",argv[1],lSourceStartPos);
		return -1;
	}
	
	pos=lseek(wfd,lDestStartPos,SEEK_SET);
	if(pos==-1)
	{
	    printf("File %s can not move pointer to %ld\n",argv[4],lDestStartPos);
		return -1;
	}
	
	i=0;
	lReadSize=0;
	buf=malloc(BLOCKSIZE);
	printf("Finished:");
	while( lReadSize < lTotalReadBytes )
	{
		if(lReadSize+ BLOCKSIZE < lTotalReadBytes)
	    {
	        lBlockSize=BLOCKSIZE;
	    }
	    else
	    {
	        lBlockSize=lTotalReadBytes - lReadSize;
	    }
		lBlockSize=read(rfd, buf, lBlockSize);
		if(lBlockSize<=0)
		{
		    break;
		}
	    
		iRet=write(wfd,buf,lBlockSize);
		if(iRet<=0)
		{
		    printf("Can not write file\n");
			break;
		}
		lReadSize=lReadSize+lBlockSize;	
		i++;
		printf("%08lldM\b\b\b\b\b\b\b\b\b",i);
	}
	printf("\n");
	close(rfd);
	close(wfd);
	free(buf);
	return 0;
	
cmdparaerror:
	printf("Syntax:  %s <sourcefile> <startpos> <readbytes> <destfile> <startpos> \n", argv[0]);
	printf("Example: %s /dev/rora_temp 4k 50M /tmp/ora_temp 0\n",argv[0]);	
	printf("Example: %s /dev/rora_temp 4k 2G  /dev/rora_temp2 4k\n",argv[0]);		
	printf("Example: %s /dev/rora_temp 4k end /tmp/ora_temp 0\n",argv[0]);	
	return -1;
	
}


off_t GetArgValue(char * szValue)
{
	int iLen=strlen(szValue);
	off_t lValue;
	int i;
	
	lValue=0;
	for(i=0;i<iLen-1;i++)
	{
		if(szValue[i]<'0' || szValue[i]>'9')
		{
		    return -1;
		}
		lValue=lValue*10+szValue[i] - '0';
	}
	
	if(szValue[i]>='0' && szValue[i]<='9')
	{
	    lValue=lValue*10+ szValue[i] - '0';
	}
	else if(szValue[i]=='k' ||	szValue[i]=='K')
	{
	    lValue=lValue*1024;
	}
	else if(szValue[i]=='m' ||	szValue[i]=='M')
    {
	    lValue=lValue*1024*1024;
    }
	else if(szValue[i]=='g' ||	szValue[i]=='G')
    {
	    lValue=lValue*1024*1024*1024;
    }
	else
	{
	    return -1;
	}
	return lValue;
}
